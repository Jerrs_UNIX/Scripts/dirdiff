# Dirdiff

Program for bash version 4. 

## About script

**Briefly:** Script is used for analysing content of directory and backup directory.

**Description of behaviour:**

- **If object exists in both directories and has equal permissions and content:**
    - script writes: ***OK \<relative path in directory\>***
- **If object exists in both directories but it hasn't same content/state or permissions:**
    - script writes: ***CHANGED \<relative path in directory\>***
- **If object exists only in checked directory:**
    - script writes: ***NEW \<relative path in directory\>***
- **If object exists only in backup directory:**
    - script writes: ***MISSING \<relative path in directory\>***

## Arguments
 
- **$1:** Contains directory which will be checked.
- **$2:** Contains directory which is backup of  directory in first argument.

## Example of usage

```bash
admin@machine ~ $ dirdiff /test /backup_test
OK a
OK a/aa
CHANGED a/aa/t.txt
MISSING a/ab
OK a/text.txt
OK b
OK b/ba
MISSING b/ba/sym_miss
CHANGED b/ba/tex.txt
CHANGED b/bb
OK b/bb/text1.txt
MISSING b/missing.txt
CHANGED rebel symlink
NEW b/bb/symlink
NEW new dir
NEW a/txt tex.txt
```