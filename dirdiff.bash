#!/usr/bin/env bash

## @file
## @brief        		Script is used for analysing content of
##                              directory and backup directory.
## 
## @details  	                If object exists in both directories and has
##                              equal permissions and content,
##                              script writes OK <relative path in directory>.
## 
##                              If object exists in both directories but it hasn't
##                              same content/state or permissions,
##                              script writes CHANGED <relative path in directory>.
## 
##                              If object exists only in checked directory,
##                              script writes NEW <relative path in directory>
## 
##                              If object exists only in backup directory,
##                              script writes MISSING <relative path in directory>
## 
##                              Example of usage:
##                              *****************
## 
##                                      dirdiff /usr /backup_usr
## 
## @arg         path $1         Contains directory which will be checked.
## @arg         path $2         Contains directory which is backup of 
##                              directory in first argument.
## 
## @author       		Jaroslav Vedral <jerrs1@seznam.cz>
## @license      		CC-BY-SA

#######################################################################
#                             Functions                               #
#######################################################################

## @brief                       Function prints usage of command
function print_usage {
    echo "
Usage:
------
    dirdiff original_dir backup_dir
"
}

## @brief                       Function prints error to error output
## @arg         string  $1      Message which will be printed to standart error output
## @retval      0               Due to error handling function returns always true
function print_error {
    echo -e "$1" 1>&2
    return 0
}

## @brief                       Function prints error to error output and usage to standart output
## @arg         string  $1      Message which will be printed to standart error output
## @retval      0               Due to error handling function returns always true
function print_error_and_usage {
    print_error "$1"
    print_usage
    return 0
}

## @brief                       Function check and sets arguments
## @arg         path    $1      Path to original directory
## @arg         path    $2      Path to backup directory
function process_arguments {
    # check existence of arguments
    [[ -z "$1" ]] && print_error_and_usage "First agument can't be empty!" && return 1
    [[ -z "$2" ]] && print_error_and_usage "Second agument can't be empty!" && return 1

    # check if arguments are directories
    ! [[ -d "$1" ]] && print_error_and_usage "First argument must be a directory!" && return 1
    ! [[ -d "$2" ]] && print_error_and_usage "Second argument must be a directory!" && return 1

    # set arguments
    origdir="$1"
    backupdir="$2"
}

## @brief                       Print information about content of folder given as argument
## @arg         path    $1      Path to folder which will be processed
## @arg         path    $2      Path to temporary file for error output created during processing
function list_dir_with_info {
    find "$1" -mindepth 1 -exec ls -dla --full-time {} + -a \( -type f -not -readable -exec cat {} \; \) 2> "$2" | tr -s " " | cut -f1,3,4,5,9- -d " "
}

## @brief                       Function prints warning to output
function store_symlink {
    # permissions can't be changed for symlink
    perms=""
    # Delete long suffix: all after -> 
    path="${1%% -\>*}"
    # Delete long prefix: all before ->
    target="${1##*-\> }"
}

## @brief                       Function prints warning to output
function fetch_processed_information {
    declare temp
    # clearing variables which are conditionally set
    size=""
    target=""
    
    perms="$(echo "$1" | cut -f1-3 -d" ")"
    [[ "$(echo "$perms" | cut -c1)" == "-" ]] && size="$(echo "$1" | cut -f4 -d" ")"
    temp="$(echo "$1" | cut -f5- -d" ")" 
    # Delete prefix: original directory => get relative path to original directory
    temp="${temp#$2/}"
    [[ "$(echo "$perms" | cut -c1)" == "l" ]] && store_symlink "$temp" || path="$temp"
}

## @brief                       Function prints warning to output
function process_changed_directory {
    processed="$(list_dir_with_info "$origdir" "$origtempfile")"

    [[ -s "$origtempfile" ]] && print_error "We have encountered errors during processing original folder:\n\n$(cat $origtempfile | cut -f2- -d " ")" && return 1

    IFS="$IFSnewline"
    for line in ${processed}; do
        fetch_processed_information "$line" "$origdir"
        dirperms["$path"]="$perms"
        dirsize["$path"]="$size"
        dirtarget["$path"]="$target"
    done
    IFS="$IFSnormal"
}

## @brief                       Function prints warning to output
function check_equality_of_files {
    [[ "$2" != "$4" ]] && return 1
    diff "$1" "$3" || return 1
    return 0
}

## @brief                       Delete item given by key in variable @link ::path @endlink from assoc arrays
function cleanup_assoc {
    # Delete item, if asoc array isn't empty after loop => these items will be reported as NEW
    unset dirperms["$path"]
    unset dirsize["$path"]
    unset dirtarget["$path"]
}

## @brief                       Function prints message to standard output
## @retval      0               Function always return true
function report {
    echo -e "$1"
    return 0
}

## @brief                       List content of backup directory and compare to stored information
##                              from original folder in variable @link ::origdir @endlink
function compare_to_backup_directory {
    processed="$(list_dir_with_info "$backupdir" "$backuptempfile")"

    [[ -s "$backuptempfile" ]] && print_error "We have encountered errors during processing backup folder:\n\n$(cat $backuptempfile | cut -f2- -d " ")" && return 1

    declare origperms
    declare origsize
    declare origtarget

   IFS="$IFSnewline" 
    for line in ${processed}; do
        fetch_processed_information "$line" "$backupdir"
        # Obtain stored information
        origperms="${dirperms["$path"]}"
        origsize="${dirsize["$path"]}"
        origtarget="${dirtarget["$path"]}"

        # if variables are empty => it missing in original directory
        [[ -z "$origperms" ]] && [[ -z "$origtarget" ]] && report "MISSING $path" && cleanup_assoc && continue
        # if permission strings are not equal => object was changed
        [[ "$origperms" != "$perms" ]] && report "CHANGED $path" && cleanup_assoc && continue
        # if path is file, check size and content
        [[ -f "$backupdir/$path" ]] && ! check_equality_of_files "$origdir/$path" "$origsize" "$backupdir/$path" "$size" && report "CHANGED $path" && cleanup_assoc && continue
        # if path is symbolic link, check target of link
        [[ -L "$backupdir/$path" ]] && [[ "$origarget" != "$target" ]] && report "CHANGED $path" && cleanup_assoc && continue

        # If permissions and content (for files) or target (for symbolic links) is fine => object is OK
        report "OK $path"
        cleanup_assoc
    done

    # Report all new objects
    for key in ${!dirperms[@]}; do
        report "NEW $key"
    done
    IFS="$IFSnormal"
}

## @brief                       Generate and store paths for temporary files
## 
## @details  	                More information can be found here: @link ::origtempfile @endlink
##                              and also here:                      @link ::backuptempfile @endlink
function init_temp_files {
    origtempfile="${TMPDIR}/.$(basename "${0}")_orig_$(date +%s)"
    backuptempfile="${TMPDIR}/.$(basename "${0}")_backup_$(date +%s)"
}

#######################################################################
# 				Main 				      #
#######################################################################

TMPDIR="/tmp"

#********************************************************************
#* for better readibility there are declarations of variables which *
#* is used in program and functions                                 *
#********************************************************************

## @brief    	Contains information about files, folders and symlinks.
##              These information will be futher processsed
declare processed
## @brief    	Contains path of file which was processed by find
declare path
## @brief    	Contains permissions and owners of file
##              in variable @link ::process_changed_directory::path @endlink
declare perms
## @brief    	Contains size of file in bytes. File is stored
##              in variable @link ::process_changed_directory::path @endlink
declare size
declare target
## @brief    	Contains path to directory, which will be checked
declare origdir
## @brief    	Contains path to backup directory of original directory
##              stored in variable @link ::origdir @endlink
declare backupdir
## @brief    	Contains path to temporary file of @link ::origdir @endlink which is used for catching
##              errors from find command during processing the previously-mentioned folder.
declare origtempfile
## @brief    	Contains path to temporary file of @link ::backupgdir @endlink which is used for catching
##              errors from find command during processing the previously-mentioned folder.
declare backuptempfile

declare IFSnormal
declare IFSnewline

IFSnormal="$IFS"
IFSnewline=$'\n'

declare -A dirperms
declare -A dirsize
declare -A dirtarget

init_temp_files

# delete temp files if error or end of script
trap "rm -f \"$origtempfile\" \"$backuptempfile\"" ERR EXIT

process_arguments "$1" "$2" || exit 1
process_changed_directory || exit 2
compare_to_backup_directory || exit 3
