#!/usr/bin/env bash

## @file
## @brief        		Script is used for setting and running tests for program in gitlab repository 
## @author       		Jaroslav Vedral <jerrs1@seznam.cz>
## @license      		CC-BY-SA

#######################################################################
#                             Functions                               #
#######################################################################

## @brief                       Function prints information about test which will be runned
##                              and run test specified by arguments
## @arg         int     $1      Number of test which will be runned
## @arg         string  $2      String keyword for identification of category of test
function run_test {
    echo "###########################################################"
    echo "Category: $2 | Number: $1"
    echo "###########################################################"

    bash "$TESTEDSCRIPTPATH" ${args[*]} > $standartoutput 2> $erroroutput
}

## @brief                       Tests script against chosen data
## @arg         int     $1      Number of tests for chosen category
## @arg         string  $2      Keyword for selecting a test
function run_tests {
    # cleanup test area
    find "$testarea" -maxdepth 1 -mindepth 1 -exec rm -Rf {} +
    # run prepare functions
    for (( i=1; i<=$1; i++ )); do
        prepare_data "$i" "$2"
        run_test "$i" "$2"
        check_test "$i" "$2"
    done
}

###############################################################################
#                     Functions - For modification                            #
#   Useful variables:                                                         #
#                               testarea       - path to testing area         #
#                               args           - array of arguments which     #
#                                                will be added to script      #
#                               standartoutput - path to standart output of   #
#                                                script                       # 
#                               erroroutput    - path to err output of script # 
###############################################################################

## @brief                       Print results of test specified by arguments
## @arg         int     $1      Number of test which was runned
## @arg         string  $2      String keyword for identification of category of test
function check_test {
    declare expstd
    declare experr
    declare outputval

    expstd="$(cat "$standartoutput")"
    experr="$(cat "$erroroutput")"

    echo -e "\nError Output is: \n\n$experr\n\n############################################################"
    echo -e "\nStandart Output is: \n\n$expstd\n\n###########################################################"
   
    [[ "$(echo -n "$expstd" | sort)" == "${expectedoutput[$2_$1]}" ]] && [[ "$(echo -n "$experr" | sort)" == "${expectederroutput[$2_$1]}" ]] && outputval="Success" || outputval="Fail"

    echo -e "Category: $2 | Number: $1 => $outputval\n"
}

## @brief                       Performs data preparation for test specified by arguments
## @arg         int     $1      Number of test which will be prepared
## @arg         string  $2      String keyword for identification of category of test
function prepare_data {
    # Declare variables
    declare origdest
    declare backupdest
    # call global preparation
    global_preparation "$1" "$2"
    # call local preparation
    case "$2" in
        "same")  
                    # nothing to edit
                    expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                        ;;
        "basics") 
                    case "$1" in
                        1)      # changed folders only
                                chmod 700 "$origdest/a"
                                chmod 770 "$origdest/b/ba"
                                expectedoutput[$2_$1]="$(echo -en "CHANGED a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nCHANGED b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        2)      # changed files only
                                chmod 600 "$origdest/a/aa/textik.txt"
                                chmod 660 "$origdest/a/text.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nCHANGED a/aa/textik.txt\nOK a/ab\nCHANGED a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        3)      # changed links only
                                ln -s "unknown_changed" "$origdest/symlink"
                                ln -s "unknown_changed" "$origdest/b/bb/symlink"
                                ln -s "unknown" "$backupdest/symlink"
                                ln -s "unknown" "$backupdest/b/bb/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nCHANGED b/bb/symlink\nOK b/bb/textovka.txt\nCHANGED symlink" | sort)"
                                    ;;
                        4)      # changed folders and files
                                chmod 700 "$origdest/b/bb"
                                chmod 400 "$origdest/b/bb/textovka.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nCHANGED b/bb\nCHANGED b/bb/textovka.txt" | sort)"
                                    ;;
                        5)      # changed folders and links
                                chmod 500 "$origdest/a/ab"
                                ln -s "unknown_changed" "$origdest/b/ba/symlink"
                                ln -s "unknown" "$backupdest/b/ba/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nCHANGED a/ab\nOK a/text.txt\nOK b\nOK b/ba\nCHANGED b/ba/symlink\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        6)      # changed files and links
                                echo "Changed" >> "$origdest/b/ba/texticek.txt"
                                ln -s "unknown_changed" "$origdest/symlink"
                                ln -s "unknown" "$backupdest/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nCHANGED b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nCHANGED symlink" | sort)"
                                    ;;
                        7)      # changed folders, files and links
                                find "$origdest" -type d -exec chmod 700 {} \; -o -type f -exec chmod 600 {} \;
                                ln -s "unknown_changed" "$origdest/b/ba/symlink"
                                ln -s "unknown" "$backupdest/b/ba/symlink"
                                expectedoutput[$2_$1]="$(echo -en "CHANGED a\nCHANGED a/aa\nCHANGED a/aa/textik.txt\nCHANGED a/ab\nCHANGED a/text.txt\nCHANGED b\nCHANGED b/ba\nCHANGED b/ba/symlink\nCHANGED b/ba/texticek.txt\nCHANGED b/bb\nCHANGED b/bb/textovka.txt" | sort)"
                                    ;;
                        8)      # missing folders only
                                rm -Rf "$origdest/a/ab"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nMISSING a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)" 
                                    ;;
                        9)      # missing files only
                                rm -f "$origdest/a/text.txt" "$origdest/b/ba/texticek.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nMISSING a/text.txt\nOK b\nOK b/ba\nMISSING b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        10)     # missing link only
                                ln -s "unknown" "$backupdest/b/ba/symlink"
                                ln -s "unknown" "$backupdest/a/aa/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nMISSING a/aa/symlink\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nMISSING b/ba/symlink\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        11)     # missing folders and files
                                rm -Rf "$origdest/a/ab" "$origdest/b/ba"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nMISSING a/ab\nOK a/text.txt\nOK b\nMISSING b/ba\nMISSING b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"
                                    ;;
                        12)     # missing folders and links
                                ln -s "unknown" "$backupdest/b/symlink"
                                rm -Rf "$origdest/a/ab"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nMISSING a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nMISSING b/symlink" | sort)" 
                                    ;;
                        13)     # missing files and links
                                ln -s "unknown" "$backupdest/a/ab/symlink"
                                rm -f "$origdest/a/text.txt" "$origdest/b/bb/textovka.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nMISSING a/ab/symlink\nMISSING a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nMISSING b/bb/textovka.txt" | sort)" 
                                    ;;
                        14)     # missing folders, files and links
                                ln -s "unknown" "$backupdest/a/ab/symlink"
                                rm -Rf "$origdest/a/text.txt" "$origdest/b"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nMISSING a/ab/symlink\nMISSING a/text.txt\nMISSING b\nMISSING b/ba\nMISSING b/ba/texticek.txt\nMISSING b/bb\nMISSING b/bb/textovka.txt" | sort)" 
                                    ;;
                        15)     # new folders only
                                mkdir "$origdest/newfolder" "$origdest/b/ba/newfol"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW newfolder\nNEW b/ba/newfol" | sort)" 
                                    ;;
                        16)     # new files only
                                touch "$origdest/a/ab/newfil"
                                echo "NEW" > "$origdest/b/file_newcome.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW b/file_newcome.txt\nNEW a/ab/newfil" | sort)"
                                    ;;
                        17)     # new links only
                                ln -s "unknown" "$origdest/b/ba/symlink"
                                ln -s "unknown" "$origdest/a/aa/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW b/ba/symlink\nNEW a/aa/symlink" | sort)"
                                    ;;
                        18)     # new folders and files
                                mkdir "$origdest/newfolder" "$origdest/b/ba/newfol"
                                rm -Rf "$backupdest/a/aa"
                                echo "NEW" > "$origdest/b/ba/txt.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW b/ba/txt.txt\nNEW a/aa/textik.txt\nNEW newfolder\nNEW a/aa\nNEW b/ba/newfol" | sort)"
                                    ;;
                        19)     # new folders and links
                                mkdir "$origdest/a/newfolder"
                                ln -s "unknown" "$origdest/b/bb/symlink"
                                ln -s "unknown" "$origdest/a/aa/syml"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW a/aa/syml\nNEW b/bb/symlink\nNEW a/newfolder" | sort)"
                                    ;;
                        20)     # new files and links
                                touch "$origdest/a/aa/newfile.txt"
                                ln -s "unknown" "$origdest/b/ba/symlink"
                                ln -s "unknown" "$origdest/a/aa/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nNEW b/ba/symlink\nNEW a/aa/symlink\nNEW a/aa/newfile.txt" | sort)"
                                    ;;
                        21)     # new folders, files and links
                                mkdir "$origdest/newfolder" "$origdest/b/ba/newfol"
                                rm -Rf "$backupdest/a/ab" "$backupdest/b/bb"
                                echo "NEW" > "$origdest/b/bb/t.txt"
                                ln -s "unknown" "$origdest/a/aa/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nNEW a/ab\nNEW newfolder\nNEW b/ba/newfol\nNEW b/bb\nNEW a/aa/symlink\nNEW b/bb/textovka.txt\nNEW b/bb/t.txt" | sort)"
                                    ;;
                    esac
                        ;;
        "permissions")  
                    case "$1" in
                        1)      # test file permission denied in changed directory
                                chmod 200 "$origdest/a/aa/textik.txt"
                                chmod 200 "$origdest/b/ba/texticek.txt"
                                expectederroutput[$2_$1]="$(echo -en "We have encountered errors during processing original folder:\n\n$(cat "$origdest/a/aa/textik.txt" 2>&1 | cut -f2- -d" ")\n$(cat "$origdest/b/ba/texticek.txt" 2>&1 | cut -f2- -d" ")" | sort)"
                                    ;;
                        2)      # test file permission denied in backup directory
                                chmod 200 "$backupdest/a/aa/textik.txt"
                                chmod 200 "$backupdest/b/ba/texticek.txt"
                                expectederroutput[$2_$1]="$(echo -en "We have encountered errors during processing backup folder:\n\n$(cat "$backupdest/a/aa/textik.txt" 2>&1 | cut -f2- -d" ")\n$(cat "$backupdest/b/ba/texticek.txt" 2>&1 | cut -f2- -d" ")" | sort)"
                                    ;;
                    esac
                        ;;
        "spaces") 
                    case "$1" in
                        1)      # changed folders only
                                mkdir "$origdest/rebel a" "$backupdest/rebel a"
                                chmod 700 "$origdest/rebel a"
                                chmod 770 "$origdest/b/ba"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nCHANGED b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt\nCHANGED rebel a" | sort)"
                                    ;;
                        2)      # changed files only
                                touch "$origdest/a/aa/file textik.txt" "$backupdest/a/aa/file textik.txt"
                                chmod 600 "$origdest/a/aa/file textik.txt"
                                chmod 660 "$origdest/a/text.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nCHANGED a/aa/file textik.txt\nOK a/aa/textik.txt\nOK a/ab\nCHANGED a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nOK b/bb/textovka.txt" | sort)"    
                                    ;;
                        3)      # changed links only
                                ln -s "unknown_changed" "$origdest/rebel symlink"
                                ln -s "unknown_changed" "$origdest/b/bb/symlink"
                                ln -s "unknown" "$backupdest/rebel symlink"
                                ln -s "unknown" "$backupdest/b/bb/symlink"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nOK a/aa/textik.txt\nOK a/ab\nOK a/text.txt\nOK b\nOK b/ba\nOK b/ba/texticek.txt\nOK b/bb\nCHANGED b/bb/symlink\nOK b/bb/textovka.txt\nCHANGED rebel symlink" | sort)"
                                    ;;
                    esac
                        ;;
        "all_in_one")  
                    case "$1" in
                        1)      # changed+new+missing files, folders, links
                                # links
                                ln -s "unknown changed" "$origdest/rebel symlink"
                                ln -s "unknown_new" "$origdest/b/bb/symlink"
                                ln -s "unknown" "$backupdest/rebel symlink"
                                ln -s "unknown missing" "$backupdest/b/ba/sym_miss"
                                #folders
                                mkdir "$origdest/new dir"
                                rmdir "$origdest/a/ab"
                                chmod 700 "$backupdest/b/bb"
                                # files
                                echo "changed" > "$origdest/a/aa/textik.txt"
                                chmod 600 "$backupdest/b/ba/texticek.txt"
                                echo "new" > "$origdest/a/txt tex.txt"
                                touch "$backupdest/b/missing.txt"
                                expectedoutput[$2_$1]="$(echo -en "OK a\nOK a/aa\nCHANGED a/aa/textik.txt\nOK a/text.txt\nOK b\nOK b/ba\nMISSING b/ba/sym_miss\nCHANGED b/ba/texticek.txt\nCHANGED b/bb\nOK b/bb/textovka.txt\nMISSING b/missing.txt\nCHANGED rebel symlink\nNEW b/bb/symlink\nNEW new dir\nNEW a/txt tex.txt\nMISSING a/ab" | sort)"
                                    ;;
                    esac    
                        ;;
    esac
}

#########################################################################
#                        Functions - User defined                       #
#########################################################################

## @brief                       Performs data preparation same for all tests
## @arg         int     $1      Number of test which will be prepared
## @arg         string  $2      String keyword for identification of category of test
function global_preparation {
    mkdir -p "$testarea"
    origdest="$testarea/test_$2_$1"
    backupdest="$testarea/backup_$2_$1"
    args=("$origdest" "$backupdest")
    cp -r "$testarea/../basedata" "$origdest"
    cp -r "$testarea/../basedata" "$backupdest"
    rm -f "$origdest/a/ab/.gitkeep"
    rm -f "$backupdest/a/ab/.gitkeep"
}

#########################################################################

#########################################################################
#                     Variables - For modification                      #
#########################################################################
CATEGORIES="same values:basics:permissions denied:names with spaces:all in one:quit"
TESTEDSCRIPTPATH="$(dirname $0)/../dirdiff.bash"
TEMPORARYFILES="/tmp"
#########################################################################

#########################################################################
#                   Global variables - User defined                     #
#########################################################################
declare -A expectedoutput
declare -A expectederroutput
#########################################################################

declare realoutput
declare testarea
declare IFSnormal
declare standartoutput
declare erroroutput

standartoutput="$TEMPORARYFILES/.unitstdoutput$(date +%s).txt"
erroroutput="$TEMPORARYFILES/.uniterroutput$(date +%s).txt"

trap "rm -f \"$standartoutput\" \"$erroroutput\"" ERR EXIT

declare -a args

IFSnormal="$IFS"
IFScolumn=$":"

temp="$(dirname "$0")"
testarea="$(echo "$temp" | grep -e "^/.\+" > /dev/null 2>&1 && echo "$temp/testarea" || echo "$(pwd)/$temp/testarea")"
unset temp

IFS="$IFScolumn"
PS3="Which tests would you like to run: "
select test in $CATEGORIES; do
    IFS="$IFSnormal"
    case $test in
#########################################################################
#                     Code - For modification                           #
#########################################################################
        "same values") run_tests 1 "same" ;;
        "basics")  run_tests 21 "basics" ;;
        "permissions denied") run_tests 2 "permissions" ;;
        "names with spaces") run_tests 3 "spaces" ;;
        "all in one")  run_tests 1 "all_in_one";;
#########################################################################
        "quit")  exit 0 ;;
    esac
done
